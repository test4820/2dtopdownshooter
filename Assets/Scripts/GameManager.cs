using TopDownShooter.Characters;
using TopDownShooter.Misc;
using TopDownShooter.Others;
using UnityEngine;

namespace TopDownShooter
{
    public sealed class GameManager : Singleton<GameManager>
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private int _defaultTargetFPS = 120;

        [Header("Components")]
        [SerializeField]
        private GameObject _spawner;

        [Header("Prefabs")]
        [SerializeField]
        private GameObject _playerPrefab;


        private IPlayer player;
        private ISpawnerController spawner;
        #endregion

        #region Props
        public IPlayer GetPlayer => player;
        #endregion

        #region Events
        #endregion




        #region Private Methods
        private void Init()
        {
            var rr = Screen.currentResolution.refreshRate + 1;
            Application.targetFrameRate = rr > 1 ? rr : _defaultTargetFPS;

            spawner = _spawner.GetComponent<ISpawnerController>();
        }


        private void SpawnPlayer()
        {
            player = Factory.getInstance.Create<IPlayer>(_playerPrefab);
        }
        #endregion

        #region Unity Methods
        protected override void Awake()
        {
            base.Awake();

            Init();
        }

        private void Start()
        {
            NewGame();
        }

        private void OnApplicationQuit()
        { }
        #endregion

        #region Public Methods
        public void NewGame()
        {
            SpawnPlayer();
            spawner.IsActive = true;
        }
        #endregion
    }
}