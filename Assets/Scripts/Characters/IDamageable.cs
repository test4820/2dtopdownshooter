﻿using System;

namespace TopDownShooter.Characters
{
    internal interface IDamageable
    {
        int GetHP { get; }

        event Action OnDie;

        void Damage(int damage);
    }
}