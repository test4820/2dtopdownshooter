﻿using UnityEngine;

namespace TopDownShooter.Characters
{
    public interface IMovable
    {
        float GetSpeed { get; }
        float GetAngularSpeed { get; }
        Vector3 Position { get; set; }
        Quaternion Rotation { get; set; }
    }
}