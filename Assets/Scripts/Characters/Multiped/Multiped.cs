using System;
using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Characters
{
    public class Multiped : MonoBehaviour, IUpdatable, IMovable, IDamageable
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private float _speed = 5f;
        [SerializeField]
        private int _hp = 100;
        [SerializeField]
        private int _score = 10;
        [SerializeField]
        private float _angularSpeed = 1f;

        [Header("Components")]
        [SerializeField]
        private Rigidbody2D _rigidbody2D;


        private AIMoveController moveController;
        private HPController hpController;
        #endregion

        #region Props
        public float GetSpeed => _speed;
        public float GetAngularSpeed => _angularSpeed;
        public Rigidbody2D GetRigidbody2D => _rigidbody2D;
        public int GetHP { get => _hp; set => _hp = value; }
        public Vector3 Position { get => _rigidbody2D.position; set => _rigidbody2D.position = value; }
        public Quaternion Rotation { get => transform.rotation; set => _rigidbody2D.SetRotation(value); }
        #endregion

        #region Events
        public event Action OnDie;
        #endregion



        #region Private Methods
        private void Init()
        {
            hpController = new HPController(this, Die);
            moveController = new AIMoveController(this, _rigidbody2D, (GameManager.getInstance.GetPlayer as Component).transform);
        }

        private void Die()
        {
            OnDie.Invoke();
            UIManager.getInstance.GetScoreController.Inc(_score);
            Destroy(gameObject);
        }
        #endregion

        #region Unity Methods
        // Start is called before the first frame update
        void Start()
        {
            Init();
        }

        private void OnEnable()
        {
            TickManager.getInstance.OnTick += FastUpdate;
            TickManager.getInstance.OnFixedTick += FastFixedUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnTick -= FastUpdate;
            TickManager.getInstance.OnFixedTick -= FastFixedUpdate;
        }
        #endregion

        #region Public Methods
        public void FastFixedUpdate()
        {
            moveController?.FastFixedUpdate();
        }

        public void FastUpdate()
        {
            moveController?.FastUpdate();
        }

        public void Damage(int damage)
        {
            hpController.Damage(damage);
        }
        #endregion
    }
}
