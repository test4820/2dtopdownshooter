using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Characters
{
    public class UserMoveController : IUpdatable
    {
        #region Fields
        private readonly IMovable movable;
        private readonly Camera cam;
        private readonly Rigidbody2D rigidbody2D;

        private Vector2 axis;
        private Vector2 mousePos;
        #endregion


        public UserMoveController(IMovable movable, Rigidbody2D rigidbody2D, Camera cam)
        {
            this.movable = movable;
            this.cam = cam;
            this.rigidbody2D = rigidbody2D;
        }


        #region Private Methods
        #endregion

        #region Public Methods
        public void FastUpdate()
        {
            axis.x = Input.GetAxisRaw("Horizontal");
            axis.y = Input.GetAxisRaw("Vertical");


            mousePos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Mathf.Abs(cam.transform.position.z)));
        }

        public void FastFixedUpdate()
        {
            rigidbody2D.MovePosition(rigidbody2D.position + axis * movable.GetSpeed * Time.fixedDeltaTime);

            Vector2 dir = mousePos - rigidbody2D.position;
            rigidbody2D.rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;
        }

        #endregion
    }
}