﻿using System;
using TopDownShooter.Misc;
using TopDownShooter.Weapons;
using UnityEngine;

namespace TopDownShooter.Characters
{
    public class UserWeaponController : IUpdatable
    {
        #region Fields
        private readonly IArmed arm;

        private int currentWeapon;
        private float wheelAxis;
        #endregion

        #region Events
        public event Action<IWeapon> OnChange;
        #endregion

        public UserWeaponController(IArmed arm, int defaultWeapon = 0)
        {
            this.arm = arm;
            this.currentWeapon = defaultWeapon;
            foreach (var weapon in arm.GetWeapons)
                weapon.IsActive = false;
        }


        #region Private Methods
        private void ChangeWeapon(int weaponIndex)
        {
            arm.GetWeapons[currentWeapon].IsActive = false;
            currentWeapon = Mathf.Clamp(weaponIndex, 0, arm.GetWeapons.Length - 1);
            arm.GetWeapons[currentWeapon].IsActive = true;
            OnChange?.Invoke(arm.GetWeapons[currentWeapon]);
        }
        #endregion

        #region Public Methods
        public void FastUpdate()
        {
            wheelAxis = Input.GetAxis("Mouse ScrollWheel");
            if (wheelAxis != 0f)
                ChangeWeapon(Mathf.CeilToInt(wheelAxis));

            for (int i = 49; i < 57; i++)
            {
                if (Input.GetKey((KeyCode)i))
                    ChangeWeapon(i - 49);
            }


            if (Input.GetKey(KeyCode.Mouse0) && arm.GetWeapons[currentWeapon].IsActive)
                arm.GetWeapons[currentWeapon].Shoot();
        }

        public void FastFixedUpdate()
        {

        }
        #endregion
    }
}