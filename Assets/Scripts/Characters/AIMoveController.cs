﻿using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Characters
{
    internal class AIMoveController : IUpdatable
    {
        #region Fields
        private readonly IMovable movable;
        private readonly Rigidbody2D rigidbody2D;
        private readonly Transform target;
        #endregion


        public AIMoveController(IMovable movable, Rigidbody2D rigidbody2D, Transform target)
        {
            this.movable = movable;
            this.rigidbody2D = rigidbody2D;
            this.target = target;
        }


        #region Methods
        public void FastFixedUpdate()
        {
            Vector2 dir = (Vector2)target.position - rigidbody2D.position;
            rigidbody2D.rotation = Mathf.LerpAngle(rigidbody2D.rotation, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f, movable.GetAngularSpeed * Time.fixedDeltaTime);
            rigidbody2D.position += (Vector2)rigidbody2D.transform.up * movable.GetSpeed * Time.fixedDeltaTime;
        }

        public void FastUpdate()
        {

        }
        #endregion
    }
}