using UnityEngine;
using UnityEngine.UI;

namespace TopDownShooter.UI
{
    public class ScoreController : MonoBehaviour, IScoreController
    {
        #region Fields
        [Header("Components")]
        [SerializeField]
        private Text _scoreText;

        private int currentScore;
        #endregion

        #region Props
        public int GetScore => currentScore;
        #endregion



        #region Private Methods
        void UpdateScore()
        {
            _scoreText.text = currentScore.ToString();
        }
        #endregion

        #region Unity Methods
        void Start()
        {
            currentScore = 0;
        }
        #endregion

        #region Public Methods
        public void Dec(int score)
        {
            currentScore -= score;
            UpdateScore();
        }

        public void Inc(int score)
        {
            currentScore += score;
            UpdateScore();
        }
        #endregion
    }
}