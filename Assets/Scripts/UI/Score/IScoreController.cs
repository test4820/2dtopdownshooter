

namespace TopDownShooter.UI
{
    public interface IScoreController
    {
        int GetScore { get; }

        void Inc(int score);
        void Dec(int score);
    }
}