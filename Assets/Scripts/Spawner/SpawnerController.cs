using TopDownShooter.Characters;
using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Others
{
    public class SpawnerController : MonoBehaviour, IUpdatable, ISpawnerController
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private float _spawnTime = 5;
        [SerializeField]
        private Transform[] _spawnPoints;

        [Header("Prefabs")]
        [SerializeField]
        private GameObject[] _spawnPrefabs;


        private CooldownHelper spawnCooldownHelper;
        #endregion

        #region Props
        public bool IsActive { get; set; }
        #endregion



        #region Private Methods
        private void Init()
        {
            spawnCooldownHelper = new CooldownHelper(_spawnTime);
        }
        #endregion

        #region Unity Methods
        // Start is called before the first frame update
        void Start()
        {
            Init();
        }

        private void OnEnable()
        {
            TickManager.getInstance.OnTick += FastUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnTick -= FastUpdate;
        }
        #endregion

        #region Public Methods
        public void FastFixedUpdate()
        {

        }

        public void FastUpdate()
        {
            if (!IsActive)
                return;

            if (!spawnCooldownHelper.Check())
                return;

            Spawn();
        }

        public void Spawn()
        {
            if (_spawnPrefabs.Length < 1)
                return;

            var ent = Factory.getInstance.Create<IDamageable>(_spawnPrefabs[Random.Range(0, _spawnPrefabs.Length)], transform, _spawnPoints[Random.Range(0, _spawnPoints.Length)]);
            ent.OnDie += Spawn;
        }
        #endregion
    }
}
