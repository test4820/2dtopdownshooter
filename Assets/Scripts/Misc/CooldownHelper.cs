﻿using UnityEngine;

namespace TopDownShooter.Misc
{
    public class CooldownHelper
    {
        #region Fields
        private readonly float cooldownTime;

        private float lastCooldownTime;
        #endregion


        #region Methods
        public CooldownHelper(float cooldownTime)
        {
            this.cooldownTime = cooldownTime;
            lastCooldownTime = Time.time;
        }

        public bool Check()
        {
            if (lastCooldownTime + cooldownTime > Time.time)
                return false;

            lastCooldownTime = Time.time;
            return true;
        }
        #endregion
    }
}
