﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TopDownShooter.Misc
{
    public static class Helper
    {
        public static T[] GetComponentsFromGameObjects<T>(GameObject[] gameObjects)
        {
            List<T> gos = new List<T>();
            foreach (var gameObject in gameObjects)
            {
                gos.Add(gameObject.GetComponent<T>());
            }
            return gos.ToArray();
        }

    }
}