﻿
namespace TopDownShooter.Misc
{
    public interface IUpdatable
    {
        abstract void FastUpdate();
        abstract void FastFixedUpdate();
    }
}