﻿using System.Threading.Tasks;
using TopDownShooter.Characters;
using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Weapons
{
    public class BeamController : IUpdatable
    {
        private readonly IBeamWeaponAdapter beamWeapon;
        private readonly LineRenderer lineRenderer;
        private readonly ICancelable cancelable;

        private bool active;
        private Vector3 finalPoint;
        private RaycastHit2D hit;
        private IDamageable curentTarget;
        private CooldownHelper damageIntervalHelper;


        public BeamController(IBeamWeaponAdapter beamWeapon, LineRenderer lineRenderer, ICancelable cancelable)
        {
            this.beamWeapon = beamWeapon;
            this.cancelable = cancelable;
            this.lineRenderer = lineRenderer;
            damageIntervalHelper = new CooldownHelper(this.beamWeapon.GetDamageInterval);
            active = false;
        }


        private void DisableBeam()
        {
            lineRenderer.gameObject.SetActive(false);
            finalPoint = lineRenderer.transform.position;
            curentTarget = null;
            active = false;
        }
        private void EnableBeam()
        {
            finalPoint = lineRenderer.transform.position;
            lineRenderer.gameObject.SetActive(true);
            active = true;
        }


        public async void ActivateLaser()
        {
            EnableBeam();

            try { await Task.Delay((int)(beamWeapon.GetFireTime * 1000), cancelable.GetCancellationToken); }
            catch { }

            DisableBeam();
        }

        public void FastUpdate()
        {
            lineRenderer.SetPosition(0, lineRenderer.transform.position);
            lineRenderer.SetPosition(1, finalPoint);

            if (damageIntervalHelper.Check())
                curentTarget?.Damage(beamWeapon.GetDamage);
        }

        public void FastFixedUpdate()
        {
            if (!active)
                return;



            if (hit = Physics2D.Raycast(lineRenderer.transform.position, lineRenderer.transform.up, 2000))
            {
                finalPoint = hit.point;


                if (curentTarget == null || !(Component)curentTarget || !ReferenceEquals(hit.collider.transform.parent != null ? hit.collider.transform.parent.gameObject : null, ((Component)curentTarget).gameObject))
                {
                    curentTarget = hit.collider.GetComponentInParent<IDamageable>();
                }
            }
            else
                finalPoint = lineRenderer.transform.up * 2000;

        }
    }
}
