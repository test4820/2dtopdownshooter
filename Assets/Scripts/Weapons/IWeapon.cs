﻿using System;

namespace TopDownShooter.Weapons
{
    public interface IWeapon
    {
        float GetFireCooldown { get; }
        float GetCooldown { get; }
        int GetDamage { get; }
        int GetAmmo { get; }
        bool IsActive { get; set; }

        event Action OnReload;

        void Shoot();
    }
}