﻿using System;
using System.Threading.Tasks;
using TopDownShooter.Misc;

namespace TopDownShooter.Weapons
{
    public class FireController
    {
        #region Fields
        private readonly IWeapon weapon;
        private readonly Action beginReloadCallback;
        private readonly ICancelable cancelable;

        private int currentAmmo;
        private bool ready;
        private CooldownHelper fireCooldownHelper;
        #endregion

        public FireController(IWeapon weapon, Action beginReloadCallback, ICancelable cancelable)
        {
            this.weapon = weapon;
            this.beginReloadCallback = beginReloadCallback;
            currentAmmo = weapon.GetAmmo;
            this.cancelable = cancelable;
            fireCooldownHelper = new CooldownHelper(this.weapon.GetFireCooldown);
            ready = true;
        }

        #region Public Methods
        public bool Fire()
        {
            if (!weapon.IsActive)
                return false;

            if (!ready)
                return false;

            if (weapon.GetAmmo > 0 && currentAmmo < 1)
            {
                Reload();
                return false;
            }

            if (!fireCooldownHelper.Check())
                return false;


            currentAmmo--;

            return true;
        }

        public async void Reload(bool instant = false)
        {
            ready = false;
            beginReloadCallback?.Invoke();

            if (!instant)
                try { await Task.Delay((int)(weapon.GetCooldown * 1000), cancelable.GetCancellationToken); }
                catch { }

            currentAmmo = weapon.GetAmmo;
            ready = true;
        }
        #endregion
    }
}